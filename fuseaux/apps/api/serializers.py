"""
django-rest-framework serializers
"""

from rest_framework import serializers

from fuseaux.apps.heroes.models import Hero
from fuseaux.apps.jobs.models import Job, JobTag


class HeroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Hero
        fields = [
            'id',
            'name'
        ]


class JobTagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = JobTag
        fields = [
            'slug',
            'label'
        ]


class JobSerializer(serializers.HyperlinkedModelSerializer):

    tags = JobTagSerializer(many=True, read_only=True)
    image = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='url'
    )

    class Meta:
        model = Job
        fields = [
            'name',
            'slug',
            'abstract',
            'description',
            'content',
            'tags',
            'background_color',
            'foreground_color',
            'image',
        ]
