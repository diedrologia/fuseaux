"""
api urls
"""

from rest_framework import routers

from django.urls import include, path

from . import views

app_name = 'api'

router = routers.DefaultRouter()
router.register(r'heroes', views.HeroViewSet)
router.register(r'jobs', views.JobViewSet)

# Wire up our API using automatic URL routing.
urlpatterns = [
    path('', include(router.urls)),
]
