from rest_framework import viewsets

from fuseaux.apps.heroes.models import Hero
from fuseaux.apps.jobs.models import Job

from .serializers import HeroSerializer, JobSerializer


class HeroViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows heroes to be viewed or edited.
    """

    queryset = Hero._default_manager.all()
    serializer_class = HeroSerializer


class JobViewSet(viewsets.ModelViewSet):
    queryset = Job._default_manager.all()
    serializer_class = JobSerializer
