"""
Administration interface for heroes
"""

from django.contrib import admin

from .models import Hero

# from django.utils.translation import ugettext_lazy as _


@admin.register(Hero)
class HeroAdmin(admin.ModelAdmin):
    """
    Hero administration
    """

    pass
