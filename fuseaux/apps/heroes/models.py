"""
Models for heroes
"""

from django.db import models
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _


class Hero(models.Model):
    """
    Hero
    """

    name = models.CharField(
        _('Name'), max_length=255
    )

    def __str__(self):
        return force_text(self.name)

    class Meta:
        verbose_name = _('Hero')
        verbose_name_plural = _('Heroes')
