from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class HeroesConfig(AppConfig):
    name = 'fuseaux.apps.heroes'
    verbose_name = _('Heroes')
