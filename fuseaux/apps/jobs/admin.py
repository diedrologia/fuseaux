"""
Administration interface for jobs
"""

from django.contrib import admin

from .models import Job, JobTag

# from django.utils.translation import ugettext_lazy as _


@admin.register(JobTag)
class JobTagAdmin(admin.ModelAdmin):
    """
    Job administration interface
    """

    fields = (
        'label',
        'slug'
    )

    prepopulated_fields = {
        'slug': ('label',),
    }


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    """
    Job administration interface
    """

    fields = (
        'name',
        'slug',
        'abstract',
        'description',
        'content',
        'tags',
        'background_color',
        'foreground_color',
        'image',
    )

    prepopulated_fields = {
        'slug': ('name',),
    }
