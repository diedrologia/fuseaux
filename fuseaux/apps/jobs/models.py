"""
Models for jobs
"""

from ckeditor.fields import RichTextField
from colorfield.fields import ColorField
from filer.fields.image import FilerImageField

from django.db import models
from django.urls import reverse
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _


class JobTag(models.Model):
    """
    A job tag
    """

    slug = models.SlugField(
        _('Slug'), unique=True
    )

    label = models.CharField(
        _('Label'), max_length=255,
        null=False, blank=False
    )

    def get_absolute_url(self):
        return reverse(
            'jobs:tag_detail',
            kwargs={'pk': self.slug}
        )

    def __str__(self):
        return force_text(self.label)

    class Meta:
        verbose_name = _('Job tag')
        verbose_name_plural = _('Job tags')


class Job(models.Model):
    """
    A job
    """

    slug = models.SlugField(
        _('Slug'), unique=True
    )

    name = models.CharField(
        _('Name'), max_length=255,
        null=False, blank=False
    )

    abstract = models.TextField(
        _('Abstract')
    )

    description = RichTextField(
        _('Description'),
        config_name='job_description'
    )

    content = RichTextField(
        _('Content'),
        config_name='default'
    )

    tags = models.ManyToManyField(
        JobTag,
        verbose_name=_('Tags'),
        related_name='jobs',
        related_query_name='job'
    )

    background_color = ColorField(
        _('Background color')
    )

    foreground_color = ColorField(
        _('Foreground color')
    )

    image = FilerImageField(
        verbose_name=_('Image'),
        blank=False, null=False,
        related_name='jobs',
        related_query_name='job',
        on_delete=models.CASCADE,
    )

    def get_absolute_url(self):
        return reverse(
            'jobs:job_detail',
            kwargs={'pk': self.slug}
        )

    def __str__(self):
        return force_text(self.name)

    class Meta:
        verbose_name = _('Job')
        verbose_name_plural = _('Jobs')
