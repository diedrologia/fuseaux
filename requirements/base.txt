pip

Django>=2.2,<3
pytz
django-environ>=0.4.5,<0.5  # check django 3 compatibility
django-redis>=4.12.1,<4.13

# dev
django-extensions>=2.2.9,<2.3
ipython>=7.15,<7.16

# fuseaux
django-filer>=1.7.1,<1.8
django-colorfield>=0.3,<0.4
django-ckeditor>=5.9,<5.10
djangorestframework>=3.11,<3.12
markdown>=3.2.2,<3.3
django-filter>=2.3,<2.4
