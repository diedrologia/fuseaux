from django.conf import settings
from django.conf.urls import include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, re_path
from django.views.static import serve

admin.autodiscover()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('fuseaux.apps.api.urls', 'api')),
    path(
        'api/auth/',
        include('rest_framework.urls', namespace='rest_framework')
    )
]

# This is only needed when using runserver.
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
        re_path(
            r'^media/(?P<path>.*)$',
            serve,
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}
        ),
    ] + staticfiles_urlpatterns() + urlpatterns
