"""
Local settings
- Run in Debug mode

- Use console backend for emails

- Add Django Debug Toolbar
- Add django-extensions as app
"""

from .local import *  # noqa

SITE_ID = 2
